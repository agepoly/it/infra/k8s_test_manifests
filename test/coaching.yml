---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: coaching
  labels:
    name: coaching
    owner: coaching
spec:
  replicas: 1
  selector:
    matchLabels:
      app: coaching
      owner: coaching
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: coaching
        owner: coaching
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      initContainers:
        - name: init-container
          image: gitlab.com/agepoly/dependency_proxy/containers/busybox
          command: ['sh', '-c', 'chown -R 33:33 /var/www/html/wp-content/']
          volumeMounts:
            - {"name":"coaching-wordpress-content-v","mountPath":"/var/www/html/wp-content/"}
      containers:
        - name: coaching
          image: wordpress
          ports:
            - containerPort: 80
          env:
            - name: WORDPRESS_DB_HOST
              valueFrom:
                secretKeyRef:
                  name: coaching-env-secrets
                  key: WORDPRESS_DB_HOST
            - name: WORDPRESS_DB_USER
              valueFrom:
                secretKeyRef:
                  name: coaching-env-secrets
                  key: WORDPRESS_DB_USER
            - name: WORDPRESS_DB_NAME
              valueFrom:
                secretKeyRef:
                  name: coaching-env-secrets
                  key: WORDPRESS_DB_NAME
            - name: WORDPRESS_DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: coaching-env-secrets
                  key: WORDPRESS_DB_PASSWORD
            - name: WORDPRESS_CONFIG_EXTRA
              value: "define(\"WP_SITEURL\", \"https://\".explode(\":\", getenv(\"HOST\"))[1].\"/\");\ndefine(\"WP_HOME\",   \"https://\".explode(\":\", getenv(\"HOST\"))[1].\"/\");\n"
            - name: HOST
              value: "coaching.test.ageptest.ch"
          volumeMounts:
            - {"name":"coaching-wordpress-content-v","mountPath":"/var/www/html/wp-content/"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: coaching.test.ageptest.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: coaching.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: coaching.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"coaching-wordpress-content-pvc"},"name":"coaching-wordpress-content-v"}

---
apiVersion: v1
kind: Service
metadata:
  name: coaching
  labels:
    name: coaching
    owner: coaching
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: coaching
      owner: coaching
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: coaching-main-domain
  labels:
    name: coaching
    owner: coaching
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.ageptest.ch-wildcard-ssl
  routes:
    - match: Host(`coaching.test.ageptest.ch`)
      kind: Rule
      services:
        - name: coaching
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: coaching-wordpress-content-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3G
