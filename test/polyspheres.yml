---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: polyspheres
  labels:
    name: polyspheres
    owner: agepoly
spec:
  replicas: 1
  selector:
    matchLabels:
      app: polyspheres
      owner: agepoly
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: polyspheres
        owner: agepoly
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: polyspheres
          image: registry.gitlab.com/agepoly/it/dev/polyspheres:latest
          ports:
            - containerPort: 3000
          env:
            - name: JWT_KEY
              valueFrom:
                secretKeyRef:
                  name: polyspheres-env-secrets
                  key: JWT_KEY
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: polyspheres-env-secrets
                  key: DB_USER
            - name: DB_NAME
              valueFrom:
                secretKeyRef:
                  name: polyspheres-env-secrets
                  key: DB_NAME
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: polyspheres-env-secrets
                  key: DB_PASSWORD
            - name: USE_PROXY
              value: "1"
            - name: DB_PORT
              value: "5432"
            - name: DB_HOST
              value: "databases"
            - name: NODE_ENV
              value: "production"
            - name: HOST
              value: "polyspheres.test.ageptest.ch:test.vote.agepoly.ch"
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 3000
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyspheres.test.ageptest.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 3000
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyspheres.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 3000
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyspheres.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10

---
apiVersion: v1
kind: Service
metadata:
  name: polyspheres
  labels:
    name: polyspheres
    owner: agepoly
spec:
  ports:
    - port: 80
      targetPort: 3000
      protocol: TCP
      name: http
  selector:
      app: polyspheres
      owner: agepoly
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: polyspheres-main-domain
  labels:
    name: polyspheres
    owner: agepoly
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.ageptest.ch-wildcard-ssl
  routes:
    - match: Host(`polyspheres.test.ageptest.ch`)
      kind: Rule
      services:
        - name: polyspheres
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: polyspheres-test.vote.agepoly.ch
  labels:
    name: polyspheres
    owner: agepoly
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.vote.agepoly.ch-cert
  routes:
    - match: Host(`test.vote.agepoly.ch`)
      kind: Rule
      services:
        - name: polyspheres
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: test.vote.agepoly.ch-cert
  labels:
    name: polyspheres
    owner: agepoly
spec:
  commonName: test.vote.agepoly.ch
  secretName: test.vote.agepoly.ch-cert
  dnsNames:
    - test.vote.agepoly.ch
  issuerRef:
    name: letsencrypt-http
    kind: Issuer

