---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kitkoach
  labels:
    name: kitkoach
    owner: coaching
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kitkoach
      owner: coaching
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: kitkoach
        owner: coaching
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: kitkoach
          image: registry.gitlab.com/agepoly/it/dev/coaching/kitkoach:latest
          ports:
            - containerPort: 8080
          env:
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: kitkoach-env-secrets
                  key: DB_USER
            - name: DB_PASS
              valueFrom:
                secretKeyRef:
                  name: kitkoach-env-secrets
                  key: DB_PASS
            - name: MAIL_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: kitkoach-env-secrets
                  key: MAIL_CLIENT_ID
            - name: MAIL_PRIVATE_KEY
              valueFrom:
                secretKeyRef:
                  name: kitkoach-env-secrets
                  key: MAIL_PRIVATE_KEY
            - name: HOST
              value: "kitkoach.test.ageptest.ch:test.kitkoach.coaching-epfl.ch"
          volumeMounts:
            - {"name":"kitkoach-images-v","mountPath":"/app/static/images"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: kitkoach.test.ageptest.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: kitkoach.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: kitkoach.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"kitkoach-images-pvc"},"name":"kitkoach-images-v"}

---
apiVersion: v1
kind: Service
metadata:
  name: kitkoach
  labels:
    name: kitkoach
    owner: coaching
spec:
  ports:
    - port: 80
      targetPort: 8080
      protocol: TCP
      name: http
  selector:
      app: kitkoach
      owner: coaching
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: kitkoach-main-domain
  labels:
    name: kitkoach
    owner: coaching
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.ageptest.ch-wildcard-ssl
  routes:
    - match: Host(`kitkoach.test.ageptest.ch`)
      kind: Rule
      services:
        - name: kitkoach
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: kitkoach-test.kitkoach.coaching-epfl.ch
  labels:
    name: kitkoach
    owner: coaching
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.kitkoach.coaching-epfl.ch-cert
  routes:
    - match: Host(`test.kitkoach.coaching-epfl.ch`)
      kind: Rule
      services:
        - name: kitkoach
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: test.kitkoach.coaching-epfl.ch-cert
  labels:
    name: kitkoach
    owner: coaching
spec:
  commonName: test.kitkoach.coaching-epfl.ch
  secretName: test.kitkoach.coaching-epfl.ch-cert
  dnsNames:
    - test.kitkoach.coaching-epfl.ch
  issuerRef:
    name: letsencrypt-http
    kind: Issuer

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: kitkoach-images-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5G
