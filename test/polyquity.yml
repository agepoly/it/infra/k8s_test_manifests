---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: polyquity
  labels:
    name: polyquity
    owner: polyquity
spec:
  replicas: 1
  selector:
    matchLabels:
      app: polyquity
      owner: polyquity
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: polyquity
        owner: polyquity
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: polyquity
          image: ghost
          ports:
            - containerPort: 2368
          env:
            - name: url
              value: "https://polyquity.agepoly.ch"
            - name: HOST
              value: "polyquity.test.ageptest.ch"
          volumeMounts:
            - {"name":"polyquity-ghost-v","mountPath":"/var/lib/ghost/content"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 2368
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyquity.test.ageptest.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 2368
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyquity.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 2368
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: polyquity.test.ageptest.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"polyquity-ghost-pvc"},"name":"polyquity-ghost-v"}

---
apiVersion: v1
kind: Service
metadata:
  name: polyquity
  labels:
    name: polyquity
    owner: polyquity
spec:
  ports:
    - port: 80
      targetPort: 2368
      protocol: TCP
      name: http
  selector:
      app: polyquity
      owner: polyquity
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: polyquity-main-domain
  labels:
    name: polyquity
    owner: polyquity
spec:
  entryPoints:
    - websecure
  tls:
    secretName: test.ageptest.ch-wildcard-ssl
  routes:
    - match: Host(`polyquity.test.ageptest.ch`)
      kind: Rule
      services:
        - name: polyquity
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: polyquity-ghost-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3G
